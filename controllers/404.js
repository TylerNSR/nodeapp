var template = require('../views/template-main');
exports.get = function(req, res) {
    res.writeHead(404, {'Content-Type':'text/html'});
    res.write(template.build("404 - Page not found", "Well this is embarassing...","<p>It appears that this page doesn't exist, sorry about that!</p>"));
    res.end();
};
